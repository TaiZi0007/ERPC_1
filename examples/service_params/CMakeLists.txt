cmake_minimum_required(VERSION 3.8)

add_executable(app1 app.c)
add_executable(service1 service.c)
target_link_libraries(app1 ${Libraries})
target_link_libraries(service1 ${Libraries})
