cmake_minimum_required(VERSION 3.8)

add_executable(app app.c)
add_executable(service service.c)
target_link_libraries(app ${Libraries})
target_link_libraries(service ${Libraries})
